// --------navbar-------
const activePage = window.location.pathname;
const navLinks = document.querySelectorAll("nav a").forEach((link) => {
  if (link.href.includes(`${activePage}`)) {
    link.classList.add("active-a");
  }
});
const navBar = document.querySelector(".navbar", ".search::placeholder");
window.addEventListener("scroll", fixNav);
function fixNav() {
  if (window.scrollY > navBar.offsetHeight + 10) {
    navBar.classList.add("active");
    navBar.classList.add("shadow");
  } else {
    navBar.classList.remove("active");
    navBar.classList.remove("shadow");
  }
}

// blur
const loadText = document.querySelector(".loading");
const bg = document.querySelector(".bg");

let load = 0;

int = setInterval(blurring, 10);

function blurring() {
  load++;

  if (load > 99) {
    clearInterval(int);
  }
  loadText.innerHTML = `${load}%`;
  loadText.style.opacity = scale(load, 0, 100, 1, 0);
  bg.style.filter = `blur(${scale(load, 0, 100, 10, 0)}px)`;
}

const scale = (num, in_min, in_max, out_min, out_max) => {
  return ((num - in_min) * (out_max - out_min)) / (in_max - in_min) + out_min;
};
// hero-service
$(document).ready(function () {
  var parallaxSlider;
  var parallaxSliderOptions = {
    speed: 1000,
    autoplay: true,
    parallax: true,
    loop: true,
    grabCursor: true,
    centeredSlides: true,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    on: {
      init: function () {
        let swiper = this;
        for (let i = 0; i < swiper.slides.length; i++) {
          $(swiper.slides[i])
            .find(".img-container")
            .attr({
              "data-swiper-parallax": 0.75 * swiper.width,
              "data-swiper-paralalx-opacity": 0.5,
            });

          $(swiper.slides[i])
            .find(".title")
            .attr("data-swiper-parallax", 0.65 * swiper.width);
          $(swiper.slides[i])
            .find(".description")
            .attr("data-swiper-parallax", 0.5 * swiper.width);
        }
      },
      resize: function () {
        this.update();
      },
    },
    navigation: {
      nextEl: ".parallax-slider .next-ctrl",
      prevEl: ".parallax-slider .prev-ctrl",
    },
  };

  parallaxSlider = new Swiper(".parallax-slider", parallaxSliderOptions);
  $(window).on("resize", function () {
    parallaxSlider.destroy();
    parallaxSlider = new Swiper(".parallax-slider", parallaxSliderOptions);
  });
});

// home

var swiper = new Swiper(".mySwiper", {
  effect: "coverflow",
  grabCursor: true,
  centeredSlides: true,
  slidesPerView: "auto",
  initialSlide: 4,
  coverflowEffect: {
    rotate: 90,
    stretch: 10,
    depth: 1000,
    modifier: 0.5,
    slideShadows: true,
  },
  pagination: {
    el: ".swiper-pagination",
  },
});

// api-movie
const API_KEY = "api_key=7c280a21c7a5df90fce80312d720ce02";
const BASE_URL = "https://api.themoviedb.org/3";
const API_URL = BASE_URL + "/discover/movie?sort_by=popularity.desc&" + API_KEY;
const API_NAME = [
  {
    adult: false,
    gender: 2,
    id: 83522,
    known_for_department: "Acting",
    name: "Tito Ortiz",
    original_name: "Tito Ortiz",
    popularity: 3.756,
    profile_path: "/ivG0rCc5FE1XWdh6CIBTWfeIgBk.jpg",
    cast_id: 6,
    character: "Derek",
    credit_id: "6131ebe6924ce50089fd5c06",
    order: 0,
  },
  {
    adult: false,
    gender: 1,
    id: 2113207,
    known_for_department: "Acting",
    name: "Katalina Viteri",
    original_name: "Katalina Viteri",
    popularity: 2.813,
    profile_path: "/npgc1LPPiRrhWvWE6wClfI9u4o9.jpg",
    cast_id: 9,
    character: "Tara Lopez",
    credit_id: "6131ec425e12000087121fdf",
    order: 1,
  },
  {
    adult: false,
    gender: 0,
    id: 3020902,
    known_for_department: "Acting",
    name: "DeAngelo Davis",
    original_name: "DeAngelo Davis",
    popularity: 2.588,
    profile_path: null,
    cast_id: 3,
    character: "Wilson",
    credit_id: "6131eb345e12000061ce2b88",
    order: 2,
  },
  {
    adult: false,
    gender: 0,
    id: 3221001,
    known_for_department: "Acting",
    name: "Jessica Chancellor",
    original_name: "Jessica Chancellor",
    popularity: 1.4,
    profile_path: "/aswDGLaQUUx8EqIZemjzLqdxrZr.jpg",
    cast_id: 1,
    character: "Zee",
    credit_id: "6131eafa2cde98002b604fc0",
    order: 3,
  },
  {
    adult: false,
    gender: 0,
    id: 3181059,
    known_for_department: "Acting",
    name: "Noah Jay Wood",
    original_name: "Noah Jay Wood",
    popularity: 0.6,
    profile_path: null,
    cast_id: 10,
    character: "Jorge",
    credit_id: "6131ec59924ce50061badb6c",
    order: 4,
  },
  {
    adult: false,
    gender: 2,
    id: 2024741,
    known_for_department: "Acting",
    name: "Barry Piacente",
    original_name: "Barry Piacente",
    popularity: 1.4,
    profile_path: "/i2CEWkenvmpiFHFNAD7f7dVk6D0.jpg",
    cast_id: 8,
    character: "Dr. Marietta",
    credit_id: "6131ec2d5e1200002dde8d8d",
    order: 5,
  },
  {
    adult: false,
    gender: 0,
    id: 3181060,
    known_for_department: "Acting",
    name: "Olivia Crosby",
    original_name: "Olivia Crosby",
    popularity: 0.6,
    profile_path: null,
    cast_id: 2,
    character: "Black Dragon",
    credit_id: "6131eb1f6cf3d5002a64fa39",
    order: 6,
  },
  {
    adult: false,
    gender: 2,
    id: 1198438,
    known_for_department: "Acting",
    name: "Jeff Marchelletta",
    original_name: "Jeff Marchelletta",
    popularity: 1.708,
    profile_path: null,
    cast_id: 7,
    character: "Dr. Abel Lopez",
    credit_id: "6131ec106cf3d5002a64fb37",
    order: 7,
  },
  {
    adult: false,
    gender: 0,
    id: 2752444,
    known_for_department: "Acting",
    name: "Stephen Wesley Green",
    original_name: "Stephen Wesley Green",
    popularity: 1.38,
    profile_path: null,
    cast_id: 4,
    character: "Cici",
    credit_id: "6131eb4dcede6900870359fb",
    order: 8,
  },
  {
    adult: false,
    gender: 1,
    id: 935266,
    known_for_department: "Acting",
    name: "Tammy Klein",
    original_name: "Tammy Klein",
    popularity: 2.066,
    profile_path: "/rpdP6Jr4YfQSySHH0AJcFfz5lou.jpg",
    cast_id: 5,
    character: "NFX robot / AI (voice)",
    credit_id: "6131eb7acede69002a102884",
    order: 9,
  },

  {
    adult: false,
    gender: 2,
    id: 30053,
    known_for_department: "Production",
    name: "David Michael Latt",
    original_name: "David Michael Latt",
    popularity: 2.974,
    profile_path: "/cBkmGgFkGFFyyE6aWDYovvaHL0g.jpg",
    credit_id: "6131eca9b3f6f500428d7b5c",
    department: "Production",
    job: "Producer",
  },
  {
    adult: false,
    gender: 1,
    id: 30071,
    known_for_department: "Acting",
    name: "Eliza Swenson",
    original_name: "Eliza Swenson",
    popularity: 2.181,
    profile_path: "/hULyGLIaANn4MtDVHZvMyKDDoGW.jpg",
    credit_id: "61ec7c09d75bd600f2fbf627",
    department: "Sound",
    job: "Music",
  },
  {
    adult: false,
    gender: 2,
    id: 79390,
    known_for_department: "Production",
    name: "Paul Bales",
    original_name: "Paul Bales",
    popularity: 1.83,
    profile_path: "/3Y28BvJ7JwjqEd8Px0mUogDlN2Z.jpg",
    credit_id: "6131ec932b8a430061381735",
    department: "Production",
    job: "Co-Producer",
  },
  {
    adult: false,
    gender: 2,
    id: 236844,
    known_for_department: "Production",
    name: "David Rimawi",
    original_name: "David Rimawi",
    popularity: 1.704,
    profile_path: "/uGrN7upcJHt4qEx6H89qKWS1dmI.jpg",
    credit_id: "61ec7bedd11e0e001d37e51c",
    department: "Production",
    job: "Executive Producer",
  },
  {
    adult: false,
    gender: 1,
    id: 1231273,
    known_for_department: "Acting",
    name: "Lauren Pritchard",
    original_name: "Lauren Pritchard",
    popularity: 2.184,
    profile_path: "/oVAnDqIFWuHp4ZBIVX82yylYfp2.jpg",
    credit_id: "61ec7b611ad93b001b29ee69",
    department: "Writing",
    job: "Screenplay",
  },
  {
    adult: false,
    gender: 0,
    id: 1562416,
    known_for_department: "Sound",
    name: "Mikel Shane Prather",
    original_name: "Mikel Shane Prather",
    popularity: 0.6,
    profile_path: null,
    credit_id: "61ec7bfa6d4c97009b71e3c0",
    department: "Sound",
    job: "Music",
  },
  {
    adult: false,
    gender: 2,
    id: 2152597,
    known_for_department: "Camera",
    name: "Marcus Friedlander",
    original_name: "Marcus Friedlander",
    popularity: 1.101,
    profile_path: null,
    credit_id: "64714002be2d4900dcda27b7",
    department: "Directing",
    job: "Director",
  },
  {
    adult: false,
    gender: 0,
    id: 2152610,
    known_for_department: "Production",
    name: "Mario N. Bonassin",
    original_name: "Mario N. Bonassin",
    popularity: 1.22,
    profile_path: null,
    credit_id: "61ec7d62a0b6b500a7588ae7",
    department: "Production",
    job: "Post Production Coordinator",
  },
  {
    adult: false,
    gender: 0,
    id: 2297777,
    known_for_department: "Production",
    name: "Kara Sullivan",
    original_name: "Kara Sullivan",
    popularity: 0.6,
    profile_path: null,
    credit_id: "61ec7d3beb64f112efd93b51",
    department: "Production",
    job: "Casting",
  },
  {
    adult: false,
    gender: 0,
    id: 2453716,
    known_for_department: "Editing",
    name: "Cameron Ames",
    original_name: "Cameron Ames",
    popularity: 0.6,
    profile_path: null,
    credit_id: "61ec7d426d4c97006c9db240",
    department: "Editing",
    job: "Editor",
  },
  {
    adult: false,
    gender: 2,
    id: 2587154,
    known_for_department: "Acting",
    name: "Joe Roche",
    original_name: "Joe Roche",
    popularity: 2.063,
    profile_path: "/k48D1AmSxC6H94AqAykBcsWqdQP.jpg",
    credit_id: "61ec7b696d4c97001b557fd8",
    department: "Writing",
    job: "Screenplay",
  },
  {
    adult: false,
    gender: 0,
    id: 3181067,
    known_for_department: "Camera",
    name: "Richard Chapelle",
    original_name: "Richard Chapelle",
    popularity: 0.6,
    profile_path: null,
    credit_id: "61ec7c10abf8e2001bba2928",
    department: "Camera",
    job: "Director of Photography",
  },
];

const IMG_PATH = "https://image.tmdb.org/t/p/w1280";

const SEARCH_API =
  'https://api.themoviedb.org/3/search/movie?api_key=7c280a21c7a5df90fce80312d720ce02&query="';

const GENRES_WITH =
  "https://api.themoviedb.org/3/discover/movie?api_key=7c280a21c7a5df90fce80312d720ce02&language=en-US&page=1&";

const genres = [
  {
    id: 28,
    name: "Action",
  },
  {
    id: 12,
    name: "Adventure",
  },
  {
    id: 16,
    name: "Animation",
  },
  {
    id: 35,
    name: "Comedy",
  },
  {
    id: 80,
    name: "Crime",
  },
  {
    id: 99,
    name: "Documentary",
  },
  {
    id: 18,
    name: "Drama",
  },
  {
    id: 10751,
    name: "Family",
  },
  {
    id: 14,
    name: "Fantasy",
  },
  {
    id: 36,
    name: "History",
  },
  {
    id: 27,
    name: "Horror",
  },
  {
    id: 10402,
    name: "Music",
  },
  {
    id: 9648,
    name: "Mystery",
  },
  {
    id: 10749,
    name: "Romance",
  },
  {
    id: 878,
    name: "Science Fiction",
  },
  {
    id: 10770,
    name: "TV Movie",
  },
  {
    id: 53,
    name: "Thriller",
  },
  {
    id: 10752,
    name: "War",
  },
  {
    id: 37,
    name: "Western",
  },
];

const main = document.getElementById("main");

const form = document.getElementById("form");

const search = document.getElementById("search");

const tagEl = document.getElementById("tag");

const found = document.getElementById("found");
var selectorGenre = [];

setGenre();

function setGenre() {
  tagEl.innerHTML = "";

  genres.forEach((genre) => {
    const t = document.createElement("div");
    t.classList.add("tag", "col-lg-3", "col-sm-6", "col-md-3");
    t.id = genre.id;
    t.innerHTML = genre.name;

    t.addEventListener("click", () => {
      if (selectorGenre.length == 0) {
        selectorGenre.push(genre.id);
      } else {
        if (selectorGenre.includes(genre.id)) {
          selectorGenre.forEach((id, idx) => {
            if (id == genre.id) {
              selectorGenre.splice(idx, 1);
            }
          });
        } else {
          selectorGenre.push(genre.id);
        }
        console.log(selectorGenre);
        getMovie(
          GENRES_WITH + "&with_genres=" + encodeURI(selectorGenre.join(","))
        );
        highlightSelection();
      }
    });
    tagEl.append(t);
  });
}

function highlightSelection() {
  const tags = document.querySelectorAll(".tag");
  tags.forEach((tag) => {
    tag.classList.remove("highlight");
  });
  clearBtn();
  if (selectorGenre.length != 0) {
    selectorGenre.forEach((id) => {
      const highlightTag = document.getElementById(id);
      highlightTag.classList.add("highlight");
    });
  }
}

function clearBtn() {
  let clearBtn = document.getElementById("clear");
  if (clearBtn) {
    clearBtn.classList.add("highlight");
  } else {
    let clear = document.createElement("div");
    clear.classList.add(
      "tag",
      "highlight",
      "col-lg-3",
      "col-sm-6",
      "col-md-3",
      "border",
      "border-1",
      "border-warning",
      "rounded-3"
    );
    clear.id = "clear";
    clear.innerText = "Clear x";
    clear.addEventListener("click", () => {
      selectorGenre = [];
      setGenre();
      getMovie(API_URL);
    });
    tagEl.append(clear);
  }
}

// Request

function getMovie(url) {
  fetch(url)
    .then((res) => res.json())
    .then((data) => {
      console.log(data.results);
      if (data.results.length !== 0) {
        showMovie(data.results);
      } else {
        main.innerHTML = `<p class="fs-1 fw-bold text-center bebas" style="letter-spacing: 5px;">No movies found</p>`;
        found.innerHTML = "";
      }
    });
}

getMovie(API_URL);
getMovie(API_NAME);

function showMovie(movies) {
  main.innerHTML = "";

  movies.map((movie) => {
    const {
      title,
      poster_path,
      vote_average,
      original_language,
      release_date,
      genre_ids,
      overview,
    } = movie;
    const movieEl = document.createElement("div");

    movieEl.classList.add("col-lg-3", "col-sm-12", "col-md-6");

    movieEl.innerHTML = `

      <div class="movie position-relative overflow-hidden w-100 mb-5">

        <div class="img-moive position-relative overflow-hidden">
          <img src="${
            poster_path
              ? IMG_PATH + poster_path
              : "http://via.placeholder.com/1080x1580"
          }" alt="${title} class="w-100 h-100">
            <div class="overview p-4">
              <div class="d-flex justify-content-between">
                  <span class="${getClassByRate(
                    vote_average
                  )}">${vote_average}</span>
              </div>
                <div class="d-flex justify-content-between ">
                  <div>
                    <p class="list-movie">Genre:</p>
                    <p class="list-movie">Release:</p>
                    <p class="list-movie">Time:</p>
                    </div>
                  <div>
                    <p class="description-movie"> ${genre_ids}</p>
                    <p class="description-movie"> ${release_date}</p>
                    <p class="description-movie"> 02 hours</p>
                    </div>
                </div>
                    <p class="list-movie mb-0 ">Overview:</p>
                    <p class="description-movie overview-des "> ${overview}</p>
            </div>
        </div>
        <div class="movie-info d-flex justify-content-between align-items-center ">
            <span class="action bebas fs-5 ps-2">${original_language}</span>
            <span class="year bebas fs-5 pt-2">${release_date.slice(
              0,
              4
            )}</span>
        </div>
        <hr class="m-0">
        <span class="moive-title fs-5 bebas ps-2 ">${title}</span>
      </div>
        `;
    main.appendChild(movieEl);
  });
}

function getClassByRate(vote) {
  if (vote >= 8) {
    return "green";
  } else if (vote >= 5) {
    return "orange";
  } else {
    return "red";
  }
}

// search-movie

form.addEventListener("submit", (e) => {
  e.preventDefault();

  const searchTerm = search.value;
  selectorGenre = [];
  highlightSelection();
  if (searchTerm && searchTerm !== "") {
    getMovie(SEARCH_API + searchTerm);

    search.value = "";
  } else {
    window.location.reload();
  }
});
